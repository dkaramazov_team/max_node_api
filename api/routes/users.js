const express = require("express");
const router = express.Router();
const UserController = require('../controllers/users');

router.post('/signup', UserController.signup);

router.post('/signin', UserController.signin);

router.delete('/:userId', UserController.remove);

module.exports = router;