
const express = require("express");
const router = express.Router();
const checkAuth = require('../middleware/check-auth');

const OrderController = require("../controllers/orders");

// Handle incoming GET requests to /orders
router.get("/", checkAuth, OrderController.get_all);

router.post("/", checkAuth, OrderController.create);

router.get("/:orderId", checkAuth, OrderController.getById);

router.delete("/:orderId", OrderController.remove);

module.exports = router;
